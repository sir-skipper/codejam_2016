FILEPATH = "E:/Downloads/C-small-attempt0.in"

def isPrime(number):
    if number <= 2:
        return False
    i = 2
    while i <= number/2:
        if number % i == 0:
            return False
        i += 1
    return True

def baseTransform(number, base):
    result = 0
    power = 0
    while number != 0:
        result += number%10 * base**power
        number /= 10
        power += 1
    return result

def isJamCoin(number):
    for base in range(2,11):
        converted_number = baseTransform(number, base)
        if isPrime(converted_number):
            return False
    return True

def generateMinimalNumber(digitsCount):
    return 2**(digitsCount-1) + 1

def convertToBinary(number):
    nr = 2
    power = 0
    while nr <= number:
        nr *= 2
        power += 1
    nr = 0
    while number != 0:
        if number / 2**power > 0:
            nr += 10**power
            number -= 2**power
        power -= 1
    return nr

def nonTrivialDivisor(number):
    if number <= 2:
        return -1
    i = 2
    while i <= number/2:
        if number % i == 0:
            return i
        i += 1
    return -1

def baselessNonTrivialDivisors(number):
    results = []
    for base in range(2,11):
        nr = baseTransform(number, base)
        results.append(str(nonTrivialDivisor(nr)))
    return results

def readAllFile(filepath = ""):
    data = []
    if filepath == "" or filepath == None:
        filepath = FILEPATH
    with open(filepath) as f:
        for line in f:
            data.append(line.strip().split())
    return data

def writeToFile(filepath = "", data = []):
    if filepath == "" or filepath == None:
        filepath = "E:/Desktop/coin_jam.out"
    testCount = 0
    with open(filepath,"w") as f:
        for datum in data:
            testCount += 1
            f.write("Case #"+str(testCount)+":\n")
            for d in datum:
                f.write(' '.join(d)+'\n')    
    return
    
def computeData(input_data):
    output_data = []
    result = []
    testCount = 0
    for data in input_data:
        if len(data) < 2:
            continue
            
        number = generateMinimalNumber(int(data[0]))
        nr_cases = int(data[1])
        i = 0
            
        while i < nr_cases:
            i += 1
            binary_number = long(convertToBinary(number))
            while isJamCoin(binary_number) != True or binary_number % 2 == 0:
                number += 1
                binary_number = convertToBinary(number)
            result.append([str(binary_number)] + baselessNonTrivialDivisors(binary_number))
            print result
            number += 1
            
        output_data.append(result)
    return output_data

def solve():
    input_data = readAllFile()
    result = computeData(input_data)
    writeToFile(data = result)
